"use strict";
import gulp                  from 'gulp';

// Utilities
import rename             from 'gulp-rename';
import plumber            from 'gulp-plumber';
import util               from 'gulp-util';
import sourcemaps         from 'gulp-sourcemaps';
import path               from 'path';
import concat             from 'gulp-concat';
import cheerio            from 'gulp-cheerio';
import tap                from 'gulp-tap';
import uglify             from 'gulp-uglify';
import fs                 from 'fs';
import contains           from 'gulp-contains';
import foreach            from 'gulp-foreach';

// Data
import swig               from 'gulp-swig';
import data               from 'gulp-data';

// Sass
import sass               from 'gulp-sass';
import sassLint           from 'gulp-sass-lint';

// Pug
import pug                from 'gulp-pug';

// ES6
import babelify           from  'babelify';
import browserify         from  'browserify';
import source             from  'vinyl-source-stream';
import glob               from  'glob';
import es                 from  'event-stream';
import buffer             from  'vinyl-buffer';
import streamify          from  'gulp-streamify';
import watchify           from  'watchify';

// Browser
import browserSync        from 'browser-sync';

// Configuration Filse
let paths                 = require('./dev/config-paths.js');
let config                = require('./dev/site-definitions.js');
let reload                = browserSync.reload;
let babelifyConfig        = {presets: ["es2015", "react"], sourceMaps:false};

//---------------------------------TASK-------------------------------------
gulp.task('pug', (done)=> {
    compileHTML();
    done();
});

gulp.task('assets', () => {
    return gulp.src('dev/assets/**/*.*', {base: './dev/assets/'})
        .pipe(gulp.dest(config.build.folder+'/assets'))
});

let compileHTML = (input) => {
    let base = 'dev/pages/';
    if (!input) {
        input = base + '**/*.pug'
    } else {
        input = base + input + '/' + input + '.pug'
    }
    for (let i = 0; i < config.language.length; i++) {
        gulp.src('./dev/multilingual-data/'+config.language[i]+'.json', { allowEmpty: true })
            .pipe(plumber())
            .pipe(tap(function(languageData, t){
                return gulp.src(input, { allowEmpty: true })
                    .pipe(plumber())
                    .pipe(data(function(file) {
                        return JSON.parse(fs.readFileSync(path.normalize('./dev/multilingual-data/'+config.language[i]+'.json')));
                      }))
                    .pipe(swig({
                        defaults: {
                          cache: false
                        }
                    }))
                    .pipe(pug({pretty: '\t', basedir: __dirname + '/dev'}))
                    .pipe(rename({dirname: (config.language.length == 1? '': config.language[i])}))
                    .pipe(cheerio({
                        run: function($, file) {
                            // Insert common style
                            var style_common = $('<link/>').attr({
                                'rel': 'stylesheet',
                                'id': 'style_common',
                                'type': 'text/css',
                                'href': (config.language.length==1?'/':'../')+'css/common.css'
                            });
                            $('head').append(style_common);

                            // Insert page style
                            var style_page = $('<link/>').attr({
                                'rel': 'stylesheet',
                                'id': 'style_page',
                                'type': 'text/css',
                                'href': (config.language.length==1?'/':'../')+'css/'+ path.basename(file.path, path.extname(file.path)) + '.css'
                            });
                            $('head').append(style_page);

                            // Insert common script
                            var script_common = $('<script/>').attr({
                                'id': 'script_common',
                                'type': 'text/javascript',
                                'src': (config.language.length==1?'/':'../')+'js/common.js'
                            });
                            $('body').append(script_common);

                            // Insert page script
                            var script_page = $('<script/>').attr({
                                'id': 'script_page',
                                'type': 'text/javascript',
                                'src': (config.language.length==1?'/':'../')+'js/'+ path.basename(file.path, path.extname(file.path)) + '.js'
                            });
                            $('body').append(script_page);
                        },
                        parserOptions: {
                            decodeEntities: false
                        }
                    }))
                    .pipe(gulp.dest(config.build.folder))
                    .pipe(reload({stream:true}));
            })) 
    }
}

let checkPage = (input,searchPug) => {
    gulp.src(input)
        .pipe(plumber())
        .pipe(contains({
            search: searchPug,
            onFound: function (string, file, cb) {
                compileHTML(path.basename(file.path, '.pug'));
            }
    }));
}

gulp.task('scss', (done) => {
    compileScss(paths.scssLibEntry, 'common.scss');
    compileScss(paths.scssEntry,'');
    done();
});

let compileScss = (input, output = '') => {
    return gulp.src(input, { allowEmpty: true })
        .pipe(plumber())
        .pipe(output==''? util.noop() : concat(output))
        .pipe(config.sourcemap? sourcemaps.init() : util.noop())
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(config.sourcemap? sourcemaps.write() : util.noop())
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(config.build.folder+'/css'))
        .pipe(browserSync.stream());  
}

//Babel Es6 Javascript Compiler
let browserifySetting = (entry)=>{
    return browserify({entries: entry,cache: {},packageCache: {},plugin: [watchify],
            ignoreWatch: ['**/node_modules/**', '**/bower_components/**']})
            .transform(babelify.configure(babelifyConfig));
}

let bundleJs = (bundler,entry,compress=false)=> {
    bundler
        .bundle()
        .pipe(source(entry))
        .pipe(config.development||notCompress?  util.noop() : streamify(uglify()))
        .pipe(compress?  streamify(uglify()) : util.noop())
        .pipe(rename({dirname: ''}))
        .pipe(rename({extname: '.js'}))
        .pipe(gulp.dest(config.build.folder+'/js'))
        .pipe(browserSync.stream());
}

gulp.task('babel-lib', (done) => {
    let bundler = browserifySetting(paths.jsLibEntry);
    bundleJs(bundler,paths.jsLibEntry,true);
    done();
});

gulp.task('babel', (done) => {
    glob(paths.jsEntry, (err, files)=>{
        if(err) done(err)
        let tasks = files.map((entry)=>{
            let bundler = browserifySetting(entry);
            return bundler
                .bundle()
                .pipe(source(entry))
                .pipe(config.development?  util.noop() : streamify(uglify()))
                .pipe(rename({dirname: ''}))
                .pipe(rename({extname: '.js'}))
                .pipe(gulp.dest(config.build.folder+'/js'))
                .pipe(browserSync.stream());
        });
        es.merge(tasks).on('end', done);
    })
    done();
});

//Check js module inclued inside the pages
let checkJs = (input,searchJs) => {
    gulp.src(input)
        .pipe(plumber())
        .pipe(contains({
            search: searchJs,
            onFound:  (string, file, cb)=> {

                let bundler = browserifySetting(file.path);
                bundleJs(bundler,file.path);
            }
    }));
}

gulp.task('default', gulp.series('pug', 'scss', 'babel-lib','babel', 'assets', function browser(done) {
    browserSync.init({
        server: {
            baseDir: config.build.folder
        }
    });

    //-------------------Watch--------------------------------------
    gulp.watch(paths.scssWatch, gulp.parallel('scss'));         //scss

    gulp.watch(paths.jsLibEntry, gulp.parallel('babel-lib'));   //lib js

    //pages js watch
    let watcherPageJs = gulp.watch(paths.jsEntry);
    watcherPageJs.on('change', (filepath, stats)=> {
        let path = './'+filepath;
        let bundler = browserifySetting(path);
        bundleJs(bundler,path);
    });

    //module js watch
    let watcherJsMODULES = gulp.watch(paths.jsModuleWatch);
    watcherJsMODULES.on('change', (filepath, stats) => {
        let nowEditJs = path.basename(filepath, '.js') + '.js';
        gulp.src(paths.jsEntry)
            .pipe(plumber())
            .pipe(foreach((stream, file)=>{
                checkJs(file.path,nowEditJs)
                return stream
            }))
    });

    //pug watch
    gulp.watch([paths.jsonEntry,paths.pugCommonModuleEntry], gulp.parallel('pug'));

    let watcherHTML = gulp.watch(paths.pugEntry);
    watcherHTML.on('change', (filepath, stats)=> {
        compileHTML(path.basename(filepath, '.pug'));
    });

    let watcherHTMLMODULES = gulp.watch(paths.pugModuleEntry);
    watcherHTMLMODULES.on('change', (filepath, stats) => {
        let nowEditPug = path.basename(filepath, '.pug') + '.pug';
        gulp.src(paths.pugEntry)
            .pipe(plumber())
            .pipe(foreach((stream, file)=>{
                checkPage(file.path,nowEditPug)
                return stream
            }))
    });
    done();
}));