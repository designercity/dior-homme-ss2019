"use strict";
let gulp = require('gulp');

// Utilities
let rename = require('gulp-rename');
let plumber = require('gulp-plumber');
let util = require('gulp-util');
let sourcemaps = require('gulp-sourcemaps');
let path = require('path');
let concat = require('gulp-concat');
let cheerio = require('gulp-cheerio');
let tap = require('gulp-tap');
let uglify = require('gulp-uglify');
let fs = require('fs');

// Data
let swig = require('gulp-swig');
let data = require('gulp-data');

// Sass
let sass = require('gulp-sass');
let sassLint = require('gulp-sass-lint');

// Pug
let pug = require('gulp-pug');

// Browser
let browserSync = require('browser-sync').create();
let reload = browserSync.reload;

// ES6
let babel = require("gulp-babel");

//Add filename in json file
let extend = require('extend');

// Configuration Filse
let md = require('./dev/module-definitions.js');
let config = require('./dev/site-definitions.js');

//Generate Index listing
let index = require('gulp-index');
let merge = require('gulp-merge-json');

gulp.task('pug', function(done) {
    compileHTML();
    compileModules();
    done();
});

//copy assets
gulp.task('assets', function() {
    return gulp.src('dev/assets/**/*.*', {base: './dev/assets/'})
        .pipe(gulp.dest(config.build.folder+'/assets'))
});

let compileModules = () =>
{
    //Merge Multi Json
    for (let i = 0; i < config.language.length; i++) {
        gulp.src(`./dev/pages/**/data/${config.language[i]}.json`)
            .pipe(merge({
                'fileName':`combined-${config.language[i]}.json`
            }))
            .pipe(gulp.dest(config.build.folder));
    }

    // return gulp.src('dev/modules/**/*.pug', {base: './dev/modules/'})
    //     .pipe(pug({pretty: '\t', basedir: __dirname + '/dev'}))
    //     .pipe(gulp.dest(config.build.folder))
    compileHTML(null, true);
}

let compileHTML = (input, isModules) => {
    let base = 'dev/pages/';
    let allJson;

    if (!input) {
        input = base + '**/*.pug'
    } else {
        input = base + input + '/' + input + '.pug'
    }
    if (isModules)  input = 'dev/modules/**/*.pug';

    for (let i = 0; i < config.language.length; i++) {
        gulp.src('./dev/multilingual-data/'+config.language[i]+'.json', { allowEmpty: true })
            .pipe(plumber())
            .pipe(tap(function(languageData, t){
                return gulp.src(input, { allowEmpty: true })
                    .pipe(plumber())
                    .pipe(data(function(file) {
                        var filename = path.basename(file.path);
                        var dataFile = filename.replace(path.extname(filename), '');
                        var thisJson = JSON.parse(fs.readFileSync(path.normalize('./dev/multilingual-data/' + config.language[i] + '.json')));

                        if (isModules)
                        {
                            var file = `${config.build.folder}/combined-${config.language[i]}.json`;
                            var compJsonFile = JSON.parse(fs.readFileSync(path.normalize(file)));

                            var exportJson;
                            exportJson = extend(exportJson, compJsonFile);
                            exportJson = extend(exportJson, thisJson);
                            
                            return exportJson;
                        }

                        var file = 'dev/pages/' + dataFile + '/data/' + config.language[i] + '.json';
                        var compJsonFile = JSON.parse(fs.readFileSync(path.normalize(file)));

                        var exportJson;
                        exportJson = extend(exportJson, compJsonFile);
                        exportJson = extend(exportJson, thisJson);

                        return exportJson;
                    }))
                    .pipe(swig({
                        defaults: {
                          cache: false
                        }
                    }))
                    .pipe(pug({pretty: '\t', basedir: __dirname + '/dev'}))
                    .pipe(rename({dirname: (config.language.length == 1? '': config.language[i])}))
                    .pipe(cheerio({
                        run: function($, file) {
                            if (isModules)
                            {
                                $.root().append('<script type="text/javascript" src="/js/common.js"></script><script type="text/javascript" src="/js/selectize.min.js"></script><link rel="stylesheet" type="text/css" href="/css/common.css">');
                            }

                            // Insert common style
                            var style_common = $('<link/>').attr({
                                'rel': 'stylesheet',
                                'id': 'style_common',
                                'type': 'text/css',
                                'href': (config.language.length==1?'/':'../')+'css/common.css'
                            });
                            $('head').append(style_common);
                            if (isModules)  $('head').append(style_common);

                            // Insert page style
                            var style_page = $('<link/>').attr({
                                'rel': 'stylesheet',
                                'id': 'style_page',
                                'type': 'text/css',
                                'href': (config.language.length==1?'/':'../')+'css/'+ path.basename(file.path, path.extname(file.path)) + '.css'
                            });
                            $('head').append(style_page);

                            // Insert common script
                            // var script_common = $('<script/>').attr({
                            //     'id': 'script_common',
                            //     'type': 'text/javascript',
                            //     'src': (config.language.length==1?'/':'../')+'js/common.js'
                            // });
                            // $('head').append(script_common);
                            $('script#common').attr('id', 'script_common').attr('type', 'text/javascript').attr('src', (config.language.length==1?'/':'../')+'js/common.js');

                            // Insert page script
                            var script_page = $('<script/>').attr({
                                'id': 'script_page',
                                'type': 'text/javascript',
                                'src': (config.language.length==1?'/':'../')+'js/'+ path.basename(file.path, path.extname(file.path)) + '.js'
                            });
                            $('head').append(script_page);
                        },
                        parserOptions: {
                            decodeEntities: false
                        }
                    }))
                    .pipe(gulp.dest(config.build.folder))
                    .pipe(reload({stream:true}));
            })) 
    }
}

gulp.task('scss', function(done) {
    // Sequeuce: Library Styles, Common Styles, Module Styles
    let input = [];
    // Include all Common Library Styles
    for (let i = 0; i<md.common.library_styles.length; i++) {
        input.push('dev/libraries/' + md.common.library_styles[i]);
    }
    // Include all Common Style in sequence
    for (let i = 0; i<md.common.styles.length; i++) {
        input.push('dev/styles/' + md.common.styles[i]);
    }
    // Include all Common Module Style
    for (let i = 0; i<md.common.modules.length; i++) {
        input.push('dev/modules/' + md.common.modules[i] + '/' + md.common.modules[i] + '.scss');
    }
    compileScss(input, 'common.scss');
    input.length = 0;

    for (let i = 0; i < Object.keys(md.page).length; i++) {
        // Include all Page Libraries Styles
        for (let j = 0; j < Object.keys(md.page[Object.keys(md.page)[i]].library_styles).length; j++) {
            input.push('dev/libraries/' + md.page[Object.keys(md.page)[i]].library_styles[j]);
        }
        // Include all Page Module Style
        for (let j = 0; j < Object.keys(md.page[Object.keys(md.page)[i]].modules).length; j++) {
            input.push('dev/modules/' + md.page[Object.keys(md.page)[i]].modules[j] + '/' + md.page[Object.keys(md.page)[i]].modules[j] + '.scss');
        }
        // Include all Page Style
        input.push('dev/pages/'+Object.keys(md.page)[i]+'/'+Object.keys(md.page)[i]+'.scss');
        compileScss(input, Object.keys(md.page)[i]+'.scss');
        input.length = 0;
    }
    done();
});

let compileScss = (input, output) => {
    return gulp.src(input, { allowEmpty: true })
        .pipe(plumber())
        // .pipe(sassLint({
        //     files: {
        //         ignore: 'dev/libraries/**/*.scss'
        //     }
        // }))
        // .pipe(sassLint.format())
        // .pipe(sassLint.failOnError())
        .pipe(concat(output))
        .pipe(config.sourcemap? sourcemaps.init(): util.noop())
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(config.sourcemap? sourcemaps.write(): util.noop())
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(config.build.folder+'/css'))
        .pipe(browserSync.stream());  
}

// Task: Babel Javascript Compile
gulp.task('babel', function(done) {
    // Sequeuce: Library Scripts, Common Script, Module Scripts
    let input = [];
    // Include all Common Library Scripts
    for (let i = 0; i<md.common.library_scripts.length; i++) {
        input.push('dev/libraries/' + md.common.library_scripts[i]);
    }
    // Include all Common Scripts in sequence
    for (let i = 0; i<md.common.scripts.length; i++) {
        input.push('dev/scripts/' + md.common.scripts[i]);
    }
    // Include all Common Module Script
    for (let i = 0; i<md.common.modules.length; i++) {
        input.push('dev/modules/' + md.common.modules[i] + '/' + md.common.modules[i] + '.js');
    }
    compileScript(input, 'common.js');
    input.length = 0;


    for (let i = 0; i < Object.keys(md.page).length; i++) {
        // Include the Page Script
        input.push('dev/pages/'+Object.keys(md.page)[i]+'/'+Object.keys(md.page)[i]+'.js');
        // Include all Page Libraries Scripts
        for (let j = 0; j < Object.keys(md.page[Object.keys(md.page)[i]].library_scripts).length; j++) {
            input.push('dev/libraries/' + md.page[Object.keys(md.page)[i]].library_scripts[j]);
        }
        // Include all Page Module Script
        for (let j = 0; j < Object.keys(md.page[Object.keys(md.page)[i]].modules).length; j++) {
            input.push('dev/modules/' + md.page[Object.keys(md.page)[i]].modules[j] + '/' + md.page[Object.keys(md.page)[i]].modules[j] + '.js');
        }
        compileScript(input, Object.keys(md.page)[i]+'.js');
        input.length = 0;
    }
    done();
});

let compileScript = (input, output) => {
    return gulp.src(input, { allowEmpty: true })
        .pipe(plumber())
        .pipe(babel())
        .pipe(concat(output))
        .pipe(rename({dirname: ''}))
        //.pipe((uglify()))
        .pipe(gulp.dest(config.build.folder+'/js'))
        .pipe(browserSync.stream());
}

//Build Index
gulp.task('html:buildIndex', function() {
    return gulp.src(['./dev/pages/**/*.pug', './dev/modules/**/*.pug'])
        .pipe(index({
            'relativePath': './dev',
            'title': 'Swire SPLinks Index page',
            'prepend-to-output': () => `<head><link rel="stylesheet" type="text/css" href="/css/listing.css"></head><body>`,
            'section-heading-template': (heading) => `<h2 class="index__section-heading">${heading}</h2>`,
            'item-template': (filepath, filename) => 
            {
                //`<li class="index__item"><a class="index__item-link" href="${filepath}.html">${filepath}.html + ${filename}</a></li>`
                var pattern = (filename.indexOf('\\') > -1)? '\\':'/';
                var pageURL = (filename.split(pattern)[1]).replace('.pug', '.html');
                return `<li class="index__item"><a class="index__item-link" href="${pageURL}" target="_blank">${pageURL}</a></li>`;
            },
            'outputFile':'./listing.html'
        }))
        .pipe(gulp.dest('./build'));
});

gulp.task('default', gulp.series('pug', 'scss', 'babel', 'assets'/*, 'html:buildIndex'*/, function browser(done) {
    browserSync.init({
        server: {
            baseDir: config.build.folder
        }
    });

    var watcherStyle = gulp.watch('dev/**/*.scss');
    watcherStyle.on('change', function(filepath, stats) { 

        // Check if the change file is Common Style or Common Module Style
        if (md.common.modules.indexOf(path.basename(filepath, '.scss')) > -1 || filepath.indexOf('styles') > -1) {
            let input = [];
            // Include all Common Library Styles
            for (let i = 0; i<md.common.library_styles.length; i++) {
                input.push('dev/libraries/' + md.common.library_styles[i]);
            }
            // Include all Common Style in sequence
            for (let i = 0; i<md.common.styles.length; i++) {
                input.push('dev/styles/' + md.common.styles[i]);
            }
            // Include all Common Module Style
            for (let i = 0; i<md.common.modules.length; i++) {
                input.push('dev/modules/' + md.common.modules[i] + '/' + md.common.modules[i] + '.scss');
            }
            compileScss(input, 'common.scss');
            input.length = 0;
            console.log('Common Style Compiled');
        };

        // Check if the change file is a Page Module Style
        if (filepath.indexOf('modules') > -1 && md.common.modules.indexOf(path.basename(filepath, '.scss')) == -1) {
            let pageArray = Object.keys(md.page);
            for (let i =0; i < pageArray.length; i++) {
                if (md.page[pageArray[i]].modules.indexOf(path.basename(filepath, '.scss')) > -1) {
                    let input = [];
                    // library style
                    for (let j = 0; j < md.page[pageArray[i]].library_styles.length; j++) {
                        input.push('dev/libraries/' + md.page[pageArray[i]].library_styles[j])
                    }
                    //module style
                    for (let j = 0; j < md.page[pageArray[i]].modules.length; j++) {
                        input.push('dev/modules/' + md.page[pageArray[i]].modules[j] + '/' + md.page[pageArray[i]].modules[j] + '.scss')
                    }
                    //page style
                    input.push('dev/pages/'+pageArray[i]+'/'+pageArray[i]+'.scss');
                    compileScss(input, pageArray[i]+'.scss');
                    input.length = 0;
                    console.log('Page Module Style Compiled');
                }
            }
        }

        // Check if the change file is Page Style
        if (filepath.indexOf('pages') > -1 ) {
            let input = [];
            // Include the related Page Module Style
            for (let i = 0; i < md.page[path.basename(filepath, '.scss')].library_styles.length; i++) {
                input.push('dev/libraries/' + md.page[path.basename(filepath, '.scss')].library_styles[i]);
            }
            // Include the related Page Module Style
            for (let i = 0; i < md.page[path.basename(filepath, '.scss')].modules.length; i++) {
                input.push('dev/modules/' + md.page[path.basename(filepath, '.scss')].modules[i] + '/' + md.page[path.basename(filepath, '.scss')].modules[i] + '.scss');
            }
            // Include the related Page Style
            input.push('dev/pages/'+path.basename(filepath, '.scss')+'/'+path.basename(filepath));
            compileScss(input, path.basename(filepath));
            input.length = 0;
            console.log('Page Style Compiled');
        }
    });

    var watcherScript = gulp.watch('dev/**/*.js');
    watcherScript.on('change', function(filepath, stats) {
        // Check if the change file is Common Script or Common Module Script
        if (md.common.modules.indexOf(path.basename(filepath, '.js')) > -1 || filepath.indexOf('scripts') > -1) {
            let input = [];
            // Include all Common Library Scripts
            for (let i = 0; i<md.common.library_scripts.length; i++) {
                input.push('dev/libraries/' + md.common.library_scripts[i]);
            }
            // Include all Common Scripts in sequence
            for (let i = 0; i<md.common.scripts.length; i++) {
                input.push('dev/scripts/' + md.common.scripts[i]);
            }
            // Include all Common Module Script
            for (let i = 0; i<md.common.modules.length; i++) {
                input.push('dev/modules/' + md.common.modules[i] + '/' + md.common.modules[i] + '.js');
            }
            compileScript(input, 'common.js');
            input.length = 0;
            console.log('Common Script Compiled');
        };

        // Check if the change file is a Page Module Script
        if (filepath.indexOf('modules') > -1 && md.common.modules.indexOf(path.basename(filepath, '.js')) == -1) {
            let pageArray = Object.keys(md.page);
            for (let i =0; i < pageArray.length; i++) {
                if (md.page[pageArray[i]].modules.indexOf(path.basename(filepath, '.js')) > -1) {
                    let input = [];
                    //library script
                    for (let j = 0; j < md.page[pageArray[i]].library_scripts.length; j++) {
                        input.push('dev/libraries/' + md.page[pageArray[i]].library_scripts[j])
                    }
                    //module script
                    for (let j = 0; j < md.page[pageArray[i]].modules.length; j++) {
                        input.push('dev/modules/' + md.page[pageArray[i]].modules[j] + '/' + md.page[pageArray[i]].modules[j] + '.js')
                    }
                    //page script
                    input.push('dev/pages/' + pageArray[i] + '/' + pageArray[i] + '.js');
                    compileScript(input, pageArray[i] + '.js');
                    input.length = 0;
                    console.log('Page Module Script Compiled: from ' + input + ' -> ' + pageArray[i] + '.js');
                }
            }
        }

        // Check if the change file is Page Script
        if (filepath.indexOf('pages') > -1 ) {
            let input = [];
            // Include the related Page Script
            input.push('dev/pages/'+path.basename(filepath, '.js')+'/'+path.basename(filepath));
            // Include all Page Libraries Scripts
            for (let i = 0; i < md.page[path.basename(filepath, '.js')].library_scripts.length; i++) {
                input.push('dev/libraries/' + md.page[path.basename(filepath, '.js')].library_scripts[i]);
            }
            // Include the related Page Module Script
            for (let i = 0; i < md.page[path.basename(filepath, '.js')].modules.length; i++) {
                input.push('dev/modules/' + md.page[path.basename(filepath, '.js')].modules[i] + '/' + md.page[path.basename(filepath, '.js')].modules[i] + '.js');
            }
            compileScript(input, path.basename(filepath));
            input.length = 0;
            console.log('Page Script Compiled');
        }
    })

    var watcherHTML = gulp.watch(['dev/**/*.pug', 'dev/multilingual-data/*.json']);
    watcherHTML.on('change', function(filepath, stats) {
        // Check if the change file is a common module or the base layout
        if (md.common.modules.indexOf(path.basename(filepath, '.pug')) > -1 || path.basename(filepath, '.pug').indexOf('layout') > -1 ) {
            compileHTML();
            console.log('Common Module HTML Compiled');
        };

        // Check if the chang efile is a data file
        if (path.dirname(filepath).indexOf('multilingual-data') > -1) {
            compileHTML();
            console.log('Data File Compiled');
        };

        // Check if the change file is a Page Module HTML
        // Just compile the page as pug will automatically include all modules
        if (filepath.indexOf('modules') > -1 && md.common.modules.indexOf(path.basename(filepath, '.pug')) == -1) {
            let pageArray = Object.keys(md.page);
            for (let i =0; i < pageArray.length; i++) {
                if (md.page[pageArray[i]].modules.indexOf(path.basename(filepath, '.pug')) > -1) {
                    compileHTML(pageArray[i]);
                    console.log('Page HTML Compiled');
                }
            }
        }

        // Check if the change file is a Page HTML
        if (filepath.indexOf('pages') > -1 ) {
            compileHTML(path.basename(filepath, '.pug'));
            console.log('Page HTML Compiled');
        }
    })
    done();
}))