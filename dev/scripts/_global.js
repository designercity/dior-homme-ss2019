let globalscripts;

class GlobalScripts {
	constructor() {
		shareSocialMedia();
	}
}

$(function()
{
	globalscripts = new GlobalScripts();
});



//Social Media Share
var shareSocialMedia = function()
{
	var url = window.location.href;

	//Facebook share
	$('a.share-fb').on('click', function(event) {
		event.preventDefault();
		var shareURL = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(url);
		window.open(shareURL,'cssSocialMediaPopup','menubar=1,resizable=1,width=500,height=500');
		return false;
	});

	//Twitter share
	// $('.social-share ul a.twitter').on('click', function(event) {
	// 	event.preventDefault();
	// 	var title = $("meta[property='og:title']").attr("content") + "\n" + $("meta[property='og:description']").attr("content");
	// 	var shareURL = 'http://twitter.com/share?text=' + encodeURIComponent(title);
	// 	window.open(shareURL,'cssSocialMediaPopup','menubar=1,resizable=1,width=500,height=500');
	// 	return false;
	// });
	
	// //Linkin share
	// $('.social-share ul a.linkedin').on('click', function(event) {
	// 	event.preventDefault();
	// 	var title = $("meta[property='og:title']").attr("content") + "\n" + $("meta[property='og:description']").attr("content");
	// 	var shareURL = 'https://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(url);
	// 	window.open(shareURL,'cssSocialMediaPopup','menubar=1,resizable=1,width=500,height=500');
	// 	return false;
	// });
}