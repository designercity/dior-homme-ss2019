module.exports = {
	"common": {
		"library_scripts": ["jquery/jquery-3.3.1.min.js", "slick/slick.min.js"],
		"library_styles": ["slick/slick.scss", "slick/slick-theme.scss"],
		"styles": ["_mixin.scss", "_config.scss", "_reset.scss","_global.scss"],
		"scripts": ["_global.js"],
		"modules": ["header", "footer"]
	},
	"page": {
		"index": {
			"library_scripts": [],
			"library_styles": [],
			"modules": []
		}
	} 
}