module.exports = {
    development: true,
    sourcemap: false, 
    language: ['en'],
    build: {
        folder: 'build'
    },
	amp: {
        compile: false,
        folder: 'amp'
    }
}