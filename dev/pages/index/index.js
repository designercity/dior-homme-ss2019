let index;
let expandContent;
let beeAnimation;

class Index
{
	constructor()
	{
		expandContent = new ExpandContent();
		beeAnimation = new BeeAnimation();
		mapInit();
	}
}

class ExpandContent
{
	constructor()
	{
		let sectionLinks = $('a.sectionLink');
		let HighlightID = 0;

		//Init
		let init = () =>
		{
			for (let i=0; i<sectionLinks.length; i++)
			{
				$(sectionLinks[i])[0].num = i;
				$(sectionLinks[i])[0].container = $(sectionLinks[i]).next('.container');
				
				$(sectionLinks[i]).on('click', function(e)
				{
					if (HighlightID == this.num)
					{
						sectionOpen((this.num == 0)?1:0);
						return false;
					}
					//console.log(this.num)
					sectionOpen(this.num);
				});
			}
			$(sectionLinks[0]).addClass('active');

			//Set Map Height
			setMapH();

			$(window).on('resize', function(e)
			{
				setMapH();
			});
		};

		let setMapH = () =>
		{
			let map = $('#map');
			let mapH = $(window).height() - $('header').height() - (54 * 2);
			map.css({'height':mapH});
		};

		let sectionOpen = (id) =>
		{
			$(sectionLinks[id]).addClass('active');
			$($(sectionLinks[id])[0].container).slideDown(300, function()
			{
				// Animation complete.
				if (id == 1)
				{
					if (beeAnimation)	beeAnimation.play();
				}
			});

			for (let i=0; i<sectionLinks.length; i++)
			{
				if (i == id)	continue;
				
				$(sectionLinks[i]).removeClass('active');
				$($(sectionLinks[i])[0].container).slideUp(300, function()
				{
					// Animation complete.
				});
			}

			HighlightID = id;
		};

		init();
	}
}

class BeeAnimation
{
	constructor()
	{
		let animatedBee;
		let beeAnimate;
		let initPos = {'x':$('#animatedBee').offset().left, 'y':$('#animatedBee').offset().top};

		//Init
		let init = () =>
		{
			YUI().use('anim', (Y) =>
			{
				animatedBee = Y.one('#animatedBee');

				beeAnimate = new Y.Anim({
					node: animatedBee,
					duration: 3,
					easing: Y.Easing.easeInOut
				});
			
				//this.play();
			});
		}

		this.startAnimate = () =>
		{
			let initX = ($(window).width() - 375) / 2 + initPos.x;
			let midX = ($(window).width() - 375) / 2 + 680;

			$('#animatedBee').css({'visibility':'visible'});

			beeAnimate.set('to', {
				curve: [ [initX,366], [midX,200], [-100,664] ] // Where 1 and 2 are curve control points, and 3 is the end point.
			});
			beeAnimate.run();
		}

		this.resetToAnimStart = () =>
		{
			animatedBee.setStyles({'left': 75, 'top': -70}); // Where x0, y0 is the animation starting point
		}

		init();
	}

	play()
	{
		//console.log('play');
		this.resetToAnimStart();
		this.startAnimate();
	}
}

$(function()
{
	index = new Index();
});