"use strict";
let gulp = require('gulp');
let rename = require('gulp-rename');
let plumber = require('gulp-plumber');
let util = require('gulp-util');
let sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
let pug = require('gulp-pug');
let path = require('path');
let browserSync = require('browser-sync').create();
let reload = browserSync.reload;
let concat = require('gulp-concat');
let swig = require('gulp-swig');
let data = require('gulp-data');
let cheerio = require('gulp-cheerio');
let inject = require('gulp-inject-string');
let tap = require('gulp-tap');
let uglify = require('gulp-uglify');
let fs = require('fs');
let stripCssComments = require('gulp-strip-css-comments');
let sassLint = require('gulp-sass-lint');
let babel = require("gulp-babel");


let md = require('./dev/module-definitions.js');
let config = require('./dev/site-definitions.js');

gulp.task('pug', function(done) {
    compileHTML();
    done();
});

//copy assets
gulp.task('assets', function() {
    return gulp.src('dev/assets/**/*.*', {base: './dev/assets/'})
        .pipe(gulp.dest(config.build.folder+'/assets'))
});

let compileHTML = (input) => {
    let base = 'dev/pages/';
    if (!input) {
        input = base + '**/*.pug'
    } else {
        input = base + input + '/' + input + '.pug'
    }
    for (let i = 0; i < config.language.length; i++) {
        gulp.src('./dev/multilingual-data/'+config.language[i]+'.json', { allowEmpty: true })
            .pipe(plumber())
            .pipe(tap(function(languageData, t){
                return gulp.src(input, { allowEmpty: true })
                    .pipe(plumber())
                    .pipe(data(function(file) {
                        return JSON.parse(fs.readFileSync(path.normalize('./dev/multilingual-data/'+config.language[i]+'.json')));
                      }))
                    .pipe(swig({
                        defaults: {
                          cache: false
                        }
                    }))
                    .pipe(pug({pretty: '\t', basedir: __dirname + '/dev'}))
                    .pipe(rename({dirname: (config.language.length == 1? '': config.language[i])}))
                    .pipe(cheerio({
                        run: function($, file) {
                            // rel for amphtml
                            if (config.amp.compile) {
                                var rel_amphtml = $('<link/>').attr({
                                    'rel': 'amphtml',
                                    'href': '../amp' + (config.language.length == 1? '': '/' + config.language[i] ) +'/' + path.basename(file.path)
                                })
                                $('head').append(rel_amphtml);
                            }

                            // Insert common style
                            var style_common = $('<link/>').attr({
                                'rel': 'stylesheet',
                                'id': 'style_common',
                                'type': 'text/css',
                                'href': (config.language.length==1?'./':'../')+'css/common.css'
                            });
                            $('head').append(style_common);

                            // Insert page style
                            var style_page = $('<link/>').attr({
                                'rel': 'stylesheet',
                                'id': 'style_page',
                                'type': 'text/css',
                                'href': (config.language.length==1?'./':'../')+'css/'+ path.basename(file.path, path.extname(file.path)) + '.css'
                            });
                            $('head').append(style_page);

                            // Insert common script
                            var script_common = $('<script/>').attr({
                                'id': 'script_common',
                                'type': 'text/javascript',
                                'src': (config.language.length==1?'./':'../')+'js/common.js'
                            });
                            $('body').append(script_common);

                            // Insert page script
                            var script_page = $('<script/>').attr({
                                'id': 'script_page',
                                'type': 'text/javascript',
                                'src': (config.language.length==1?'./':'../')+'js/'+ path.basename(file.path, path.extname(file.path)) + '.js'
                            });
                            $('body').append(script_page);
                        },
                        parserOptions: {
                            decodeEntities: false
                        }
                    }))
                    .pipe(gulp.dest(config.build.folder))
                    .pipe(reload({stream:true}));
            })) 
    }
}

gulp.task('scss', function(done) {
    // Sequeuce: Library Styles, Common Styles, Module Styles
    let input = [];
    // Include all Common Library Styles
    for (let i = 0; i<md.common.library_styles.length; i++) {
        input.push('dev/libraries/' + md.common.library_styles[i]);
    }
    // Include all Common Style in sequence
    for (let i = 0; i<md.common.styles.length; i++) {
        input.push('dev/styles/' + md.common.styles[i]);
    }
    // Include all Common Module Style
    for (let i = 0; i<md.common.modules.length; i++) {
        input.push('dev/modules/' + md.common.modules[i] + '/' + md.common.modules[i] + '.scss');
    }
    compileScss(input, 'common.scss');
    input.length = 0;

    for (let i = 0; i < Object.keys(md.page).length; i++) {
        // Include all Page Style
        input.push('dev/pages/'+Object.keys(md.page)[i]+'/'+Object.keys(md.page)[i]+'.scss');
        // Include all Page Libraries Styles
        for (let j = 0; j < Object.keys(md.page[Object.keys(md.page)[i]].library_styles).length; j++) {
            input.push('dev/libraries/' + md.page[Object.keys(md.page)[i]].library_styles[j]);
        }
        // Include all Page Module Style
        for (let j = 0; j < Object.keys(md.page[Object.keys(md.page)[i]].modules).length; j++) {
            input.push('dev/modules/' + md.page[Object.keys(md.page)[i]].modules[j] + '/' + md.page[Object.keys(md.page)[i]].modules[j] + '.scss');
        }
        compileScss(input, Object.keys(md.page)[i]+'.scss');
        input.length = 0;
    }
    done();
});

let compileScss = (input, output) => {
    return gulp.src(input, { allowEmpty: true })
        .pipe(plumber())
        .pipe(sassLint({
            files: {
                ignore: 'dev/libraries/**/*.scss'
            }
        }))
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError())
        .pipe(concat(output))
        .pipe(config.sourcemap? sourcemaps.init(): util.noop())
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(config.sourcemap? sourcemaps.write(): util.noop())
        .pipe(rename({dirname: ''}))
        .pipe(gulp.dest(config.build.folder+'/css'))
        .pipe(browserSync.stream());  
}

// Task: Babel Javascript Compile
gulp.task('babel', function(done) {
    // Sequeuce: Library Scripts, Common Script, Module Scripts
    let input = [];
    // Include all Common Library Scripts
    for (let i = 0; i<md.common.library_scripts.length; i++) {
        input.push('dev/libraries/' + md.common.library_scripts[i]);
    }
    // Include all Common Scripts in sequence
    for (let i = 0; i<md.common.scripts.length; i++) {
        input.push('dev/scripts/' + md.common.scripts[i]);
    }
    // Include all Common Module Script
    for (let i = 0; i<md.common.modules.length; i++) {
        input.push('dev/modules/' + md.common.modules[i] + '/' + md.common.modules[i] + '.js');
    }
    compileScript(input, 'common.js');
    input.length = 0;


    for (let i = 0; i < Object.keys(md.page).length; i++) {
        // Include the Page Script
        input.push('dev/pages/'+Object.keys(md.page)[i]+'/'+Object.keys(md.page)[i]+'.js');
        // Include all Page Libraries Scripts
        for (let j = 0; j < Object.keys(md.page[Object.keys(md.page)[i]].library_scripts).length; j++) {
            input.push('dev/libraries/' + md.page[Object.keys(md.page)[i]].library_scripts[j]);
        }
        // Include all Page Module Script
        for (let j = 0; j < Object.keys(md.page[Object.keys(md.page)[i]].modules).length; j++) {
            input.push('dev/modules/' + md.page[Object.keys(md.page)[i]].modules[j] + '/' + md.page[Object.keys(md.page)[i]].modules[j] + '.js');
        }
        compileScript(input, Object.keys(md.page)[i]+'.js');
        input.length = 0;
    }
    done();
});

let compileScript = (input, output) => {
    return gulp.src(input, { allowEmpty: true })
        .pipe(plumber())
        .pipe(babel())
        .pipe(concat(output))
        .pipe(rename({dirname: ''}))
        .pipe((uglify()))
        .pipe(gulp.dest(config.build.folder+'/js'))
        .pipe(browserSync.stream());
}

function buildAMPFolder(done) {
    if (config.amp.compile) {
        return gulp.src([config.build.folder+'/**/*.*'])
            .pipe(gulp.dest(config.amp.folder));
    } else {
        done(new Error('AMP is not set to compile. Please set in site-definition.js'))
    }
    
}
exports.buildAMPFolder = buildAMPFolder;
function convertAMPFormat(done) {
    return gulp.src(config.amp.folder+'/**/*.html')
        .pipe(plumber())
        .pipe(cheerio({
            run: function($, file) {
                var filePathArray = path.dirname(file.path).split(path.sep);
                // Replace html tag to html amp tag
                $('html').attr('amp', '');

                // Remove Scripts
                $('script').remove();

                // Convert all page css to inline
                let ampCustomStyle = $('<style>').attr({
                    'amp-custom': ''
                }).appendTo($('head'));
                $('link[rel="stylesheet"]').each(function() {
                    // Remove all comments
                    gulp.src(config.amp.folder+'/css/'+path.basename($(this).attr('href')))
                    .pipe(stripCssComments())
                    .pipe(gulp.dest(config.amp.folder+'/css'));
                    let data = fs.readFileSync(path.normalize(config.amp.folder+'/css/'+path.basename($(this).attr('href')))).toString();
                    ampCustomStyle.append(data)
                });
                
                // Append Form Library if form tag is found
                if ($('form').length > 0) {
                    $('form').each(function() {
                        if ($(this).attr('target') === undefined) {
                            $(this).attr('target', '_top');
                        }
                        if ($(this).attr('method') === undefined) {
                            $(this).attr('method', 'GET');
                            $(this).attr('action', '#');
                            if ($(this).attr('method') == 'POST') {
                                var actionValue = $(this).attr('action');
                                $(this).attr('action-xhr', actionValue);
                            }
                        }
                    })
                    // Inject AMP Form Library to first meta data
                    $('<script/>').attr({
                        async: '',
                        'custom-element': 'amp-form',
                        src: 'https://cdn.ampproject.org/v0/amp-form-0.1.js'
                    }).insertAfter($('meta[charset]'));
                }

                // Remove Stylesheets
                $('link[rel="stylesheet"]').remove();

                // Change Viewport Attribute
                $('meta[name="viewport"]').attr({
                    content: 'width=device-width, minimum-scale=1, initial-scale=1, user-scalable=no'
                });

                // Inject AMP Library to first meta data
                $('<script/>').attr({
                    async: '',
                    src: 'https://cdn.ampproject.org/v0.js'
                }).insertAfter($('meta[charset]'));

                // Change Canonical Link Or Create
                if ($('link[rel="amphtml"]').length > 0) {
                    $('link[rel="amphtml"]').attr({
                        rel: 'canonical',
                        href: (config.language.length == 1? '../': '../../'+filePathArray[filePathArray.length-1]+'/')+path.basename(file.path)  
                    });
                } else {
                    $('<link/>').attr({
                        rel: 'canonical',
                        href: '../'+path.basename(file.path)
                    }).appendTo($('head'));
                }

                // Remove all a href="javascript:;"
                $('a[href="javascript:;"]').removeAttr('href');

                // Replace all img tag to amp-img tag
                $('img').each(function () {
                    var ampimg = $('<amp-img>');
                    for (var i = 0; i < Object.keys($(this).attr()).length; i++) {
                        ampimg.attr(Object.keys($(this).attr())[i], Object.values($(this).attr())[i])
                        ampimg.attr({
                            layout: 'fill'
                        })
                    }
                    
                    $(this).replaceWith(ampimg);
                });
            },
            parserOptions: {
                decodeEntities: false
            }
        }))
        .pipe(inject.before('</head>','\t<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>\n\t'))
        .pipe(gulp.dest(config.amp.folder))
}
exports.convertAMPFormat = convertAMPFormat;
gulp.task('amp', gulp.series(buildAMPFolder, convertAMPFormat));

gulp.task('default', gulp.series('pug', 'scss', 'babel', 'assets', function browser(done) {
    browserSync.init({
        server: {
            baseDir: config.build.folder
        }
    });

    var watcherStyle = gulp.watch('dev/**/*.scss');
    watcherStyle.on('change', function(filepath, stats) { 

        // Check if the change file is Common Style or Common Module Style
        if (md.common.modules.indexOf(path.basename(filepath, '.scss')) > -1 || filepath.indexOf('styles') > -1) {
            let input = [];
            // Include all Common Library Styles
            for (let i = 0; i<md.common.library_styles.length; i++) {
                input.push('dev/libraries/' + md.common.library_styles[i]);
            }
            // Include all Common Style in sequence
            for (let i = 0; i<md.common.styles.length; i++) {
                input.push('dev/styles/' + md.common.styles[i]);
            }
            // Include all Common Module Style
            for (let i = 0; i<md.common.modules.length; i++) {
                input.push('dev/modules/' + md.common.modules[i] + '/' + md.common.modules[i] + '.scss');
            }
            compileScss(input, 'common.scss');
            input.length = 0;
            console.log('Common Style Compiled');
        };

        // Check if the change file is a Page Module Style
        if (filepath.indexOf('modules') > -1 && md.common.modules.indexOf(path.basename(filepath, '.scss')) == -1) {
            let pageArray = Object.keys(md.page);
            for (let i =0; i < pageArray.length; i++) {
                if (md.page[pageArray[i]].modules.indexOf(path.basename(filepath, '.scss')) > -1) {
                    let input = [];
                    input.push('dev/pages/'+pageArray[i]+'/'+pageArray[i]+'.scss');
                    for (let j = 0; j < md.page[pageArray[i]].modules.length; j++) {
                        input.push('dev/modules/' + md.page[pageArray[i]].modules[j] + '/' + md.page[pageArray[i]].modules[j] + '.scss')
                    }
                    compileScss(input, pageArray[i]+'.scss');
                    input.length = 0;
                    console.log('Page Module Style Compiled');
                }
            }
        }

        // Check if the change file is Page Style
        if (filepath.indexOf('pages') > -1 ) {
            let input = [];
            // Include the related Page Style
            input.push('dev/pages/'+path.basename(filepath, '.scss')+'/'+path.basename(filepath));
            // Include the related Page Module Style
            for (let i = 0; i < md.page[path.basename(filepath, '.scss')].modules.length; i++) {
                input.push('dev/modules/' + md.page[path.basename(filepath, '.scss')].modules[i] + '/' + md.page[path.basename(filepath, '.scss')].modules[i] + '.scss');
            }
            compileScss(input, path.basename(filepath));
            input.length = 0;
            console.log('Page Style Compiled');
        }
    });

    var watcherScript = gulp.watch('dev/**/*.ts');
    watcherScript.on('change', function(filepath, stats) {
        // Check if the change file is Common Script or Common Module Script
        if (md.common.modules.indexOf(path.basename(filepath, '.ts')) > -1 || filepath.indexOf('scripts') > -1) {
            let input = [];
            // Include all Common Library Scripts
            for (let i = 0; i<md.common.library_scripts.length; i++) {
                input.push('dev/libraries/' + md.common.library_scripts[i]);
            }
            // Include all Common Scripts in sequence
            for (let i = 0; i<md.common.scripts.length; i++) {
                input.push('dev/scripts/' + md.common.scripts[i]);
            }
            // Include all Common Module Script
            for (let i = 0; i<md.common.modules.length; i++) {
                input.push('dev/modules/' + md.common.modules[i] + '/' + md.common.modules[i] + '.ts');
            }
            compileScript(input, 'common.ts');
            input.length = 0;
            console.log('Common Script Compiled');
        };

        // Check if the change file is a Page Module Script
        if (filepath.indexOf('modules') > -1 && md.common.modules.indexOf(path.basename(filepath, '.ts')) == -1) {
            let pageArray = Object.keys(md.page);
            for (let i =0; i < pageArray.length; i++) {
                if (md.page[pageArray[i]].modules.indexOf(path.basename(filepath, '.ts')) > -1) {
                    let input = [];
                    input.push('dev/pages/'+pageArray[i]+'/'+pageArray[i]+'.ts');
                    for (let j = 0; j < md.page[pageArray[i]].modules.length; j++) {
                        input.push('dev/modules/' + md.page[pageArray[i]].modules[j] + '/' + md.page[pageArray[i]].modules[j] + '.ts')
                    }
                    compileScript(input, pageArray[i]+'.ts');
                    input.length = 0;
                    console.log('Page Module Script Compiled');
                }
            }
        }

        // Check if the change file is Page Script
        if (filepath.indexOf('pages') > -1 ) {
            let input = [];
            // Include the related Page Script
            input.push('dev/pages/'+path.basename(filepath, '.ts')+'/'+path.basename(filepath));
            // Include the related Page Module Script
            for (let i = 0; i < md.page[path.basename(filepath, '.ts')].modules.length; i++) {
                input.push('dev/modules/' + md.page[path.basename(filepath, '.ts')].modules[i] + '/' + md.page[path.basename(filepath, '.ts')].modules[i] + '.ts');
            }
            compileScript(input, path.basename(filepath));
            input.length = 0;
            console.log('Page Script Compiled');
        }
    })

    var watcherHTML = gulp.watch(['dev/**/*.pug', 'dev/multilingual-data/*.json']);
    watcherHTML.on('change', function(filepath, stats) {
        // Check if the change file is a common module or the base layout
        if (md.common.modules.indexOf(path.basename(filepath, '.pug')) > -1 || path.basename(filepath, '.pug').indexOf('layout') > -1 ) {
            compileHTML();
            console.log('Common Module HTML Compiled');
        };

        // Check if the chang efile is a data file
        if (path.dirname(filepath).indexOf('multilingual-data') > -1) {
            compileHTML();
            console.log('Data File Compiled');
        };

        // Check if the change file is a Page Module HTML
        // Just compile the page as pug will automatically include all modules
        if (filepath.indexOf('modules') > -1 && md.common.modules.indexOf(path.basename(filepath, '.pug')) == -1) {
            let pageArray = Object.keys(md.page);
            for (let i =0; i < pageArray.length; i++) {
                if (md.page[pageArray[i]].modules.indexOf(path.basename(filepath, '.pug')) > -1) {
                    compileHTML(pageArray[i]);
                    console.log('Page HTML Compiled');
                }
            }
        }

        // Check if the change file is a Page HTML
        if (filepath.indexOf('pages') > -1 ) {
            compileHTML(path.basename(filepath, '.pug'));
            console.log('Page HTML Compiled');
        }
    })
    done();
}))